FROM golang:1.20.7-alpine as build

WORKDIR /app

RUN apk update && apk add samba-dc krb5

COPY . .
RUN go mod download

RUN go build -o windns main.go

FROM qbeacco/samba-tool

WORKDIR /app

COPY --from=build /app/windns /app/windns

EXPOSE 8080

ENTRYPOINT [ "/app/windns" ]