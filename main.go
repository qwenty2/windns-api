package main

import (
	"fmt"
	"os"
	"os/exec"

	"github.com/gin-gonic/gin"
)

func executeDNSCommand(c *gin.Context, operation string, dnsServer string, zone, recordType, recordName, data, username, password string) {
	cmd := exec.Command("samba-tool", "dns", operation, dnsServer, zone, recordName, recordType, data, "-U", username+"%"+password)
	cmd.Stdout = os.Stdout
	cmd.Stderr = os.Stderr

	err := cmd.Run()
	if err != nil {
		c.JSON(500, gin.H{"error": fmt.Sprintf("Error %s DNS record: %s", operation, err)})
		return
	}

	c.JSON(200, gin.H{"message": fmt.Sprintf("DNS record %sd successfully", operation)})
}

func addDNSRecord(c *gin.Context) {
	var data struct {
		DNS        string `json:"dns"`
		Zone       string `json:"zone"`
		RecordType string `json:"type"`
		RecordName string `json:"name"`
		Data       string `json:"data"`
		Username   string `json:"username"`
		Password   string `json:"password"`
	}

	if err := c.BindJSON(&data); err != nil {
		c.JSON(400, gin.H{"error": "Invalid JSON data"})
		return
	}

	executeDNSCommand(c, "add", data.DNS, data.Zone, data.RecordType, data.RecordName, data.Data, data.Username, data.Password)
}

func deleteDNSRecord(c *gin.Context) {
	var data struct {
		DNS        string `json:"dns"`
		Zone       string `json:"zone"`
		RecordType string `json:"type"`
		RecordName string `json:"name"`
		Data       string `json:"data"`
		Username   string `json:"username"`
		Password   string `json:"password"`
	}

	if err := c.BindJSON(&data); err != nil {
		c.JSON(400, gin.H{"error": "Invalid JSON data"})
		return
	}

	executeDNSCommand(c, "delete", data.DNS, data.Zone, data.RecordType, data.RecordName, data.Data, data.Username, data.Password)
}

func main() {
	r := gin.Default()

	r.POST("/add", addDNSRecord)
	r.DELETE("/delete", deleteDNSRecord)

	r.Run(":8080")
}
